'use strict';

// I. Les boucles

/**
 * Exercice 1.1
 * 
 * Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :
 * 
 *  - l'afficher
 *  - incrémenter de 1
 */
export const exo_1_1 = () => {
	console.log("Ex1");
	for (let i=0; i<=10; i++ ){
		
		console.log(i);
	  }}


/**
 * Exercice 2.1
 * 
 * Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :
 *
 * - multiplier la première variable avec la deuxième
 * - afficher le résultat
 * - incrémenter la première variable
 */
export const exo_1_2 = () => {
	console.log("Ex2");
	let a=prompt('numero de 1 à 100');

for (let i=0; i<20; i++){

console.log(a*i);
}}


export const exo_1_3 = () => {
	console.log("Ex3");let a=prompt('numero de 1 à 100');

for (let i=100; i>=10; i--){

console.log(a*i);
}}


export const exo_1_4 = () => {
	console.log("Ex4");for (let i=1; i<=10; i=i+(i*1/2)){

	console.log(i);
	}}

	export const exo_1_5 = () => {
		console.log("Ex5");for (let i=0; i<15; i++){

		console.log("On y arrive presque...");
		}}


		export const exo_1_6 = () => {
			console.log("Ex6");
			function chaineNumbre(){
			var a=prompt('Ton nom');
			var b=prompt('Prenom');
			var c=prompt('age');
			console.log('Bonjour ' +a+ ' ' +b+ ' tu as ' +c+ ' ans');
		  }
		  chaineNumbre();}

		  export const exo_1_7 = () => {
			console.log("Ex7");for (let i=1; i<100; i=i+15){
			console.log(i + " On tient le bon bout...");
		  }}

		  export const exo_1_8 = () => {
			  console.log("Ex8");
			for (let i=200; i>0; i=i-12){
			console.log(i+ " Enfin")
		  }}