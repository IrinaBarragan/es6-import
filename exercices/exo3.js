'use strict';

// II. Les fonctions

/**
 * Exercice 2.1
 *
 * Faire une fonction qui retourne true.
 */
export const exo_3_1 = () => {
	console.log("Ex1");
    let mois=['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin', 'juillet', 'âout', 'semptembre', 'octobre', 'novembre', 'decembre'];
    console.log(mois);
}


/**
 * Exercice 2.2
 * 
 * Faire une fonction qui prend en paramètre une chaine de caractères et qui retourne cette même chaine.
 */
export const exo_3_2 = () => {
    console.log("Ex2");
    let mois=['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin', 'juillet', 'âout', 'semptembre', 'octobre', 'novembre', 'decembre'];
console.log(mois[2]);

	  
}

export const exo_3_3 = () => {
	console.log("Ex3");
	let mois=['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin', 'juillet', 'âout', 'semptembre', 'octobre', 'novembre', 'decembre'];
console.log(mois[5]);
	  }
	  



export const exo_3_4 = () => {
	console.log("Ex4");
	
    let mois=['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin', 'juillet', 'aout', 'semptembre', 'octobre', 'novembre', 'decembre'];
    let a=mois.indexOf('aout');
    mois.splice(a,1, 'âout');
    
    console.log(mois[a]);
	  }
	  


export const exo_3_5 = () => {
	console.log("Ex5");
    let dep={"02" : "Aisne", "59" :"Nord", "60":"Oise", "62":"Pas-de-Calais", "80":"Somme"}
}

export const exo_3_6 = () => {
    let dep={"02" : "Aisne", "59" :"Nord", "60":"Oise", "62":"Pas-de-Calais", "80":"Somme"}

    console.log(dep[80]);
}

export const exo_3_7 = () => {
	console.log("Ex7");
    let dep={"02" : "Aisne", "59" :"Nord", "60":"Oise", "62":"Pas-de-Calais", "80":"Somme"}
    dep[51]="Reims";
    console.log(dep);
	  
	
}

export const exo_3_8 = () => {
	console.log("Ex8");
    let mois=['janvier', 'fevrier', 'mars', 'avril', 'may', 'juin', 'juillet', 'âout', 'semptembre', 'octobre', 'novembre', 'decembre'];
    for (let i=0; i<mois.length; i++){
    console.log(mois[i]);}
}
export const exo_3_9 = () => {
	console.log("Ex9");
    let dep={"02" : "Aisne", "59" :"Nord", "60":"Oise", "62":"Pas-de-Calais", "80":"Somme"}
dep[51]="Reims";
for (var key in dep){

console.log(dep[key])}
}
export const exo_3_10 = () => {
	console.log("Ex10");
    let dep={"02" : "Aisne", "59" :"Nord", "60":"Oise", "62":"Pas-de-Calais", "80":"Somme"}
dep[51]="Reims";
for (var key in dep){

console.log("Le département " + dep[key] + "a le numéro " + key)}}