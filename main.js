'use strict';

/**
 * Imports
 */

 // Soit on importe tout
import * as exo1 from './exercices/exo1.js';
import * as exo2 from './exercices/exo2.js';
import * as exo3 from './exercices/exo3.js';
// Soit on choisi ce qu'on importe



/**
 * I. Les boucles
 */

// Exercice 1.1
exo1.exo_1_1();

// Exercice 1.2
exo1.exo_1_2();

// Exercice 1.3
exo1.exo_1_3();
// Exercice 1.4
exo1.exo_1_4();
// Exercice 1.5
exo1.exo_1_5();
// Exercice 1.6
exo1.exo_1_6();
// Exercice 7.7
exo1.exo_1_7();
// Exercice 1.8
exo1.exo_1_8();
// ...

/**
 * II. Les fonctions
 */

// Exercice 2.1
exo2.exo_2_1();

// Exercice 2.2
exo2.exo_2_2();

// Exercice 2.3
exo2.exo_2_3();
// Exercice 2.4
exo2.exo_2_4();
// Exercice 2.5
exo2.exo_2_5();
// Exercice 2.6
exo2.exo_2_6();
// Exercice 2.7
exo2.exo_2_7();
// Exercice 2.8
exo2.exo_2_8();

// ...

// Exercice 3.1
exo3.exo_3_1();

// Exercice 2.2
exo3.exo_3_2();

// Exercice 2.3
exo3.exo_3_3();
// Exercice 2.4
exo3.exo_3_4();
// Exercice 2.5
exo3.exo_3_5();
// Exercice 2.6
exo3.exo_3_6();
// Exercice 2.7
exo3.exo_3_7();
// Exercice 2.8
exo3.exo_3_8();
exo3.exo_3_9();
// Exercice 2.10
exo3.exo_3_10();

// ...
